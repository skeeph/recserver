from . import db
from .user import User


class Eatery(db.Model):
    __tablename__ = 'eatery'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(100), nullable=False, server_default='')
    open_time = db.Column(db.Time)
    close_time = db.Column(db.Time)
    latitude = db.Column(db.Float, server_default='42.966667')
    longitude = db.Column(db.Float, server_default='47.483333')
    address = db.Column(db.String(200), nullable=True)
    photos = db.relationship('EateryPhoto', backref='eatery',
                             lazy='dynamic')

    def __unicode__(self):
        return self.name


class EateryPhoto(db.Model):
    __tablename__ = 'eatery_photo'
    id = db.Column(db.Integer(), primary_key=True)
    eateryId = db.Column(db.Integer(), db.ForeignKey('eatery.id'))
    photo = db.Column(db.String(255), nullable=False)

    def __unicode__(self):
        return self.photo


class Feedback(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    rating = db.Column(db.Integer(), default=5, nullable=False)
    text = db.Column(db.String(2048), server_default='', nullable=False)
    userId = db.Column(db.Integer(), nullable=False)
    eatraryId = db.Column(db.Integer(), nullable=False)

    def __unicode__(self):
        return "%s - %s - %s" % (self.userId, self.eatraryId, self.rating)