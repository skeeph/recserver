# coding=utf-8
from flask import Flask
from flask.ext.user import SQLAlchemyAdapter, UserManager

from models import db
from config import Config
import flask_admin as admin

from models.user import User

app = Flask(__name__)
app.config.from_object(Config)
db.init_app(app)
adm = admin.Admin(app, name=u'Рекомендательный сервис', template_mode='bootstrap3')

db_adapter = SQLAlchemyAdapter(db, User)
user_manager = UserManager(db_adapter, app)
