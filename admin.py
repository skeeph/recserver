# coding=utf-8
from flask import request, url_for, redirect
from flask_admin.contrib import sqla
from flask_login import current_user

from models import eatery


class EateryAdmin(sqla.ModelView):
    column_labels = dict(name=u'Название', open_time=u'Время открытия', close_time=u'Время закрытия',
                         latitude=u'Широта', longitude=u'Долгота', address=u'Адрес')

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('user.login', next=request.url))

    def __init__(self, session):
        # Just call parent class with predefined model.
        super(EateryAdmin, self).__init__(eatery.Eatery, session, name=u'Заведения')


class PhotoAdmin(sqla.ModelView):
    column_labels = dict(photo=u'Путь к фотографии', eatery=u'Столовая')

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('user.login', next=request.url))

    def __init__(self, session):
        # Just call parent class with predefined model.
        super(PhotoAdmin, self).__init__(eatery.EateryPhoto, session, name=u'Фотографии')


class FeedBackAdmin(sqla.ModelView):
    column_labels = dict(rating=u'Рейтинг', text=u'Текст отзыва', userId=u'ID пользователя', eatraryId=u'ID столовой')

    def is_accessible(self):
        return current_user.is_authenticated

    def inaccessible_callback(self, name, **kwargs):
        # redirect to login page if user doesn't have access
        return redirect(url_for('user.login', next=request.url))

    def __init__(self, session):
        # Just call parent class with predefined model.
        super(FeedBackAdmin, self).__init__(eatery.Feedback, session, name=u'Отзывы')
