# coding=utf-8
class Config(object):
    DEBUG = True
    SECRET_KEY = 'adsgafbzcxfvbdfsbss'

    SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://eatery:eatery@localhost/eatery'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'

    USER_SEND_PASSWORD_CHANGED_EMAIL = False
    USER_SEND_USERNAME_CHANGED_EMAIL = False
    USER_SEND_REGISTERED_EMAIL = True
    USER_APP_NAME = u'Рекомендательный сервис'
    USER_ENABLE_USERNAMES = False
    USER_ENABLE_CHANGE_USERNAME = False
    USER_ENABLE_CONFIRM_EMAIL = False
