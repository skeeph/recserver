# coding=utf-8
from appInit import *
from flask import render_template, jsonify, request
from admin import *
from models import eatery,user


@app.route('/')
def hello_world():
    return render_template('index.html')


@app.route('/etrary', methods=['GET'])
def getEtrary():
    eateries = eatery.Eatery.query.all()
    res = []
    for i in eateries:
        k = dict(
            id=i.id,
            address=i.address,
            latitude=i.latitude,
            longitude=i.longitude,
            name=i.name,
            open_time=str(i.open_time),
            close_time=str(i.close_time),
            photos=[]
        )
        for j in i.photos:
            k['photos'].append(j.photo)
        res.append(k)
    return jsonify(dict(res=res, ok=True))


@app.route('/feedback/<int:eatery_id>', methods=['GET'])
def getFeedback(eatery_id):
    res = []
    feedBack = eatery.Feedback.query.filter_by(eatraryId=eatery_id).all()

    for i in feedBack:
        k = dict(
            text=i.text,
            userId=i.userId,
            rating=i.rating,
            email=User.query.get(i.userId).email
        )
        res.append(k)
    return jsonify({'res': res, 'ok': True})


@app.route('/feedback/<int:eatery_id>', methods=['POST'])
def setFeedback(eatery_id):
    data=request.get_json()
    if data['key']:
        key = data['key']
    else:
        return jsonify(dict(ok=False, message='Provide key'))

    if data['rating']:
        rating = data['rating']
    else:
        return jsonify(dict(ok=False, message='Provide Rating'))

    if data['text']:
        text = data['text']
    else:
        return jsonify(dict(ok=False, message='Provide Message'))

    idd = user_manager.verify_token(key, float('inf'))[2]
    if idd is None:
        return jsonify(dict(
            ok=False,
            message='Invalid API Token'
        ))

    user = User.query.filter_by(id=idd).first()
    if user is None:
        return jsonify(dict(
            ok=False,
            message='Invalid API Token'
        ))

    try:
        f = eatery.Feedback(text=text, userId=user.id, eatraryId=eatery_id, rating=rating)
        db.session.add(f)
        db.session.commit()
    except Exception, e:
        return jsonify(dict(ok=False, message='DB Error. ' + e.message))

    return jsonify(dict(ok=True))


@app.route('/auth', methods=['POST'])
def auth():
    if request.form['email']:
        email = request.form['email']
    else:
        return jsonify(dict(ok=False, message='Provide email'))

    if request.form['pass']:
        password = request.form['pass']
    else:
        return jsonify(dict(ok=False, message='Provide password'))

    user = user_manager.find_user_by_email(email)[0]
    if not user:
        return jsonify(dict(ok=False,
                            msg='user not found'))
    if not user_manager.verify_password(password, user):
        return jsonify(dict(ok=False,
                            msg='Invalid password'))
    return jsonify(dict(ok=True,
                        key=user.get_auth_token()))


adm.add_view(EateryAdmin(db.session))
adm.add_view(PhotoAdmin(db.session))
adm.add_view(FeedBackAdmin(db.session))

if __name__ == '__main__':
    with app.app_context():
        # db.drop_all()
        db.create_all()
        pass
    app.run(host='0.0.0.0', port=80)
